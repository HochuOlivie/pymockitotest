def any(*args):
    return MockTypes(*args)


class MockTypes:
    def __init__(self, *args):
        self.types = args

    def __iter__(self):
        return iter(self.types)


def when(_):
    global last_mock
    obj, meth, args = last_mock
    if last_mock is None:
        raise Exception(
            """You should pass mock object inside when(). Example:
when(mock.get_articles()).then_return(articles)"""
        )
    _functions[obj][meth]["count"] -= 1
    return __When(last_mock)


class __When:
    def __init__(self, mock):
        self.mock = mock

    def then_return(self, value):
        global _functions
        obj, meth, args = self.mock
        _functions[obj][meth][args] = {"action": "return", "value": value}

    def then_throw(self, exception):
        global _functions
        obj, meth, args = self.mock
        _functions[obj][meth][args] = {"action": "throw", "value": exception}


def mock(obj):
    return _Mock(obj)


class _Mock:
    def __init__(self, obj):
        global _functions
        try:
            _functions
        except NameError:
            _functions = {}
        _functions[self] = {}
        self.inst = obj

    def __getattr__(self, f):
        parent_class = self

        class MockFunction:
            def __new__(cls, *args):

                def arg_matches(args, arg_dict):
                    """
                    Function to match args passed by user
                    :param args: user's args
                    :param arg_dict: dict, where keys are recorded args
                    :return: key for the matching args or False
                    """
                    for i in arg_dict.keys():
                        check = True
                        if len(args) != len(i):
                            continue
                        for j, k in zip(args, i):
                            if type(k) == MockTypes:
                                if type(j) not in k:
                                    check = not check
                                    break
                            elif j != k:
                                check = not check
                                break
                        if check:
                            return i
                    return False

                global last_mock, _functions

                last_mock = (parent_class, f, args)
                if f in _functions[parent_class]:
                    if m_args := arg_matches(args, _functions[parent_class][f]):
                        _functions[parent_class][f]['count'] += 1
                        if _functions[parent_class][f][m_args]["action"] == 'return':
                            return _functions[parent_class][f][m_args]['value']
                        elif _functions[parent_class][f][m_args]['action'] == 'throw':
                            raise _functions[parent_class][f][m_args]['value']
                _functions[parent_class].setdefault(f, {})
                _functions[parent_class][f].setdefault('count', 0)
                _functions[parent_class][f]['count'] += 1
                return None

            def assert_called_once():
                assert _functions[parent_class][f]['count'] == 1

            def assert_called():
                assert _functions[parent_class][f]['count'] > 0

        if f not in dir(self.inst):
            raise AttributeError("No such attribute in the base class")

        return MockFunction
