from mocking import *


class SomeClass:
    def __init__(self):
        self.list = list()

    def get(self, item):
        def f():
            print("aza")

        return self.list[item]

    def __getattr__(self, item):
        return None


mockedList = mock(SomeClass())

# stubbing appears before the actual execution
when(mockedList.get(0)).then_return("first")

# the following prints "first"
print(mockedList.get(0))

mockedList.get.assert_called()
mockedList.get.assert_called_once()

# the following prints "None" because get(999) was not stubbed
print(mockedList.get(999))

# Fails because of !=1 calls to get
# mockedList.get.assert_called_once()

# this will throw AttributeError exception, no method 'get1' in SomeClass
# when(mockedList.get1(0)).then_return("first1")

when(mockedList.get(any(float, str))).then_throw(Exception)
# throws Exception
# mockedList.get(0.5)
